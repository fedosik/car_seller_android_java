package pl.vlados.ui.activity;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.vlados.entity.Brand;
import pl.vlados.entity.CarOffer;
import pl.vlados.entity.Model;
import pl.vlados.enums.BodyType;
import pl.vlados.enums.CarOfferStatus;
import pl.vlados.enums.CountryProduction;
import pl.vlados.enums.Drive;
import pl.vlados.enums.FuelType;
import pl.vlados.enums.Transmission;

import static org.junit.Assert.*;
import static pl.vlados.ui.activity.MainActivity.brandService;
import static pl.vlados.ui.activity.MainActivity.carOfferService;
import static pl.vlados.ui.activity.MainActivity.modelService;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
}