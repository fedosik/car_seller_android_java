package pl.vlados.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import pl.vlados.ui.activity.R;
import pl.vlados.entity.CarOffer;

public class CarOfferAdapter extends BaseAdapter {

    private Context ctx;
    private List<CarOffer> carOffers;
    private LayoutInflater layoutInflater;

    public CarOfferAdapter(Context ctx, List<CarOffer> carOffers) {
        this.ctx = ctx;
        this.carOffers = carOffers;
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return carOffers.size();
    }

    @Override
    public Object getItem(int position) {
        return carOffers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list, parent, false);
        }

        CarOffer co = carOffers.get(position);

        TextView title = view.findViewById(R.id.vTitle);
        TextView year = view.findViewById(R.id.vYear);
        TextView engine = view.findViewById(R.id.vEngine);
        TextView transmission = view.findViewById(R.id.vTransmission);

        //title.setText(co.getBrand() + " " + co.getModel());
        //year.setText(co.getYear());
        //engine.setText(co.getFuel());
        //transmission.setText("Manual");

        view.setTag("car tag position=" + position);

        return view;
    }
}
