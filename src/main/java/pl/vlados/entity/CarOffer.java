package pl.vlados.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.vlados.enums.BodyType;
import pl.vlados.enums.CarOfferStatus;
import pl.vlados.enums.CountryProduction;
import pl.vlados.enums.Drive;
import pl.vlados.enums.FuelType;
import pl.vlados.enums.Transmission;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CarOffer extends AbstractEntity {

    private long price;
    private long milleage;
    private CarOfferStatus status;
    private boolean negociation;
    private int year;
    private FuelType fuelType;
    private BodyType bodyType;
    private CountryProduction country;
    private Transmission transmission;
    private double engineSize;
    private short power;
    private Drive drive;
    private byte placeAmount;
    private boolean damaged;
    private boolean negotiation;
    private long model_id;
    private String description;

    public static final String TABLE_NAME_CAROFFER = "car_offer";

    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_MILLEAGE = "milleage";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_NEGOCIATION = "negociation";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_FUEL = "fuel";
    public static final String COLUMN_BODY = "body";
    public static final String COLUMN_COUNTRY = "country";
    public static final String COLUMN_TRANSMISSION = "transmission";
    public static final String COLUMN_ENGINESIZE = "engineSize";
    public static final String COLUMN_POWER = "power";
    public static final String COLUMN_DRIVE = "drive";
    public static final String COLUMN_PLACEAMOUNT = "placeAmount";
    public static final String COLUMN_DAMAGED = "damaged";
    public static final String COLUMN_NEGOTIATION = "negotiation";
    public static final String COLUMN_MODELID = "model_id";
    public static final String COLUMN_DESCRIPTION = "description";

    public static final String CREATE_TABLE_CAROFFER = "CREATE TABLE " + TABLE_NAME_CAROFFER + " (" +
            AbstractEntity.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            AbstractEntity.COLUMN_DATE_CREATED + " DATETIME," +
            AbstractEntity.COLUMN_DATE_MODIFICATED + " DATETIME," +
            COLUMN_PRICE + " REAL, " +
            COLUMN_MILLEAGE + " DECIMAL(10,3), " +
            COLUMN_STATUS + " VARCHAR, " +
            COLUMN_NEGOCIATION + " BOOLEAN, " +
            COLUMN_YEAR + " INTEGER, " +
            COLUMN_FUEL + " VARCHAR, " +
            COLUMN_BODY + " VARCHAR, " +
            COLUMN_COUNTRY + " VARCHAR, " +
            COLUMN_TRANSMISSION + " VARCHAR, " +
            COLUMN_ENGINESIZE + " REAL, " +
            COLUMN_POWER + " INTEGER, " +
            COLUMN_DRIVE + " VARCHAR, " +
            COLUMN_PLACEAMOUNT + " INTEGER, " +
            COLUMN_DAMAGED + " BOOLEAN, " +
            COLUMN_NEGOTIATION + " BOOLEAN, " +
            COLUMN_MODELID + " INTEGER, " +
            COLUMN_DESCRIPTION + " VARCHAR, " +
            " FOREIGN KEY (" + COLUMN_MODELID + ") REFERENCES " + Model.TABLE_NAME_MODEL + "(" + AbstractEntity.COLUMN_ID + "));";

    @Override
    public String toString() {
        return "CarOffer{" +
                "price=" + price +
                ", milleage=" + milleage +
                ", status=" + status +
                ", negociation=" + negociation +
                ", year=" + year +
                ", fuelType=" + fuelType +
                ", bodyType=" + bodyType +
                ", country=" + country +
                ", transmission=" + transmission +
                ", engineSize=" + engineSize +
                ", power=" + power +
                ", drive=" + drive +
                ", placeAmount=" + placeAmount +
                ", damaged=" + damaged +
                ", model_id=" + model_id +
                ", id=" + id +
                '}';
    }
}
