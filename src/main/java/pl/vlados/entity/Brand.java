package pl.vlados.entity;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.vlados.ui.custom.CustomSpinnerMarker;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Brand extends AbstractEntity implements CustomSpinnerMarker {

    private String title;
    private Set<Model> modelSet;

    public static final String TABLE_NAME_BRAND = "brand";

    public static final String COLUMN_TITLE = "title";

    public static final String CREATE_TABLE_BRAND = "CREATE TABLE " + TABLE_NAME_BRAND + " (" +
                                            AbstractEntity.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                                            AbstractEntity.COLUMN_DATE_CREATED + " DATETIME," +
                                            AbstractEntity.COLUMN_DATE_MODIFICATED + " DATETIME," +
                                            COLUMN_TITLE + " VARCHAR(30) UNIQUE NOT NULL)";


    public Brand(LocalDate dateCreated, LocalDate dateModificated, String title){
        this.dateCreated = dateCreated;
        this.dateModificated = dateModificated;
        this.title = title;
    }

    public static final List<Brand> brandList = null;

    @Override
    public String toString() {
        return title;
    }
}
