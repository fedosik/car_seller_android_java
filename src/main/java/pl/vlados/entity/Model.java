package pl.vlados.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.vlados.ui.custom.CustomSpinnerMarker;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Model extends AbstractEntity implements CustomSpinnerMarker {

    private String title;
    private long brand_id;

    public static final String TABLE_NAME_MODEL = "model";

    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_BRAND_ID = "brand_id";

    public static final String CREATE_TABLE_MODEL = "CREATE TABLE " + TABLE_NAME_MODEL + " (" +
                                            AbstractEntity.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                                            AbstractEntity.COLUMN_DATE_CREATED + " DATETIME," +
                                            AbstractEntity.COLUMN_DATE_MODIFICATED + " DATETIME," +
                                            COLUMN_TITLE + " VARCHAR(30) UNIQUE NOT NULL, " +
                                            COLUMN_BRAND_ID + " INTEGER, " +
                                            " FOREIGN KEY (" + COLUMN_BRAND_ID + ") REFERENCES " + Brand.TABLE_NAME_BRAND + "(" + AbstractEntity.COLUMN_ID + "))";

    @Override
    public String toString() {
        return title;
    }
}
