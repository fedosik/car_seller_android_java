package pl.vlados.entity;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractEntity{

    protected long id;
    protected LocalDate dateCreated;
    protected LocalDate dateModificated;

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_DATE_CREATED = "dateCreated";
    public static final String COLUMN_DATE_MODIFICATED = "dateModificated";

}
