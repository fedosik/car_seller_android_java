package pl.vlados.utils;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
@AllArgsConstructor
public class FilterHelper implements Parcelable {

    private String brand;
    private String model;
    private String bodyType;
    private Integer priceFrom;
    private Integer priceTo;
    private String fuel;
    private String transmission;
    private Integer yearFrom;
    private Integer yearTo;
    private Integer milleageFrom;
    private Integer milleageTo;
    private Integer powerFrom;
    private Integer powerTo;
    private String drive;
    private Double engineSizeFrom;
    private Double engineSizeTo;
    private String damaged;

    protected FilterHelper(Parcel in) {
        Bundle bundle = in.readBundle(getClass().getClassLoader());
        if (bundle != null) {
            brand = bundle.getString("brand", "");
            model = bundle.getString("model", "");
            bodyType = bundle.getString("bodyType", null);
            priceFrom = bundle.getInt("priceFrom", 0);
            priceTo = bundle.getInt("priceTo", 0);
            fuel = bundle.getString("fuel", "");
            transmission = bundle.getString("transmission", "");
            yearFrom = bundle.getInt("yearFrom", 0);
            yearTo = bundle.getInt("yearTo", 0);
            milleageFrom = bundle.getInt("miallegeFrom", 0);
            milleageTo = bundle.getInt("miallegeTo", 0);
            powerFrom = bundle.getInt("powerFrom", 0);
            powerTo = bundle.getInt("powerTo", 0);
            drive = bundle.getString("drive", "");
            engineSizeFrom = bundle.getDouble("engineSizeFrom", 0);
            engineSizeTo = bundle.getDouble("engineSizeTo", 0);
            damaged = bundle.getString("damaged", "ALL");
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        Bundle bundle = new Bundle();
        bundle.putString("brand", getBrand());
        bundle.putString("model", getModel());
        bundle.putString("bodyType", getBodyType());
        if (getPriceFrom() != null) {
            bundle.putInt("priceFrom", getPriceFrom());
        }
        if (getPriceTo() != null) {
            bundle.putInt("priceTo", getPriceTo());
        }
        bundle.putString("fuel", getFuel());
        bundle.putString("transmission", getTransmission());
        if (getYearFrom() != null) {
            bundle.putInt("yearFrom", getYearFrom());
        }
        if (getYearTo() != null) {
            bundle.putInt("yearTo", getYearTo());
        }
        if (getMilleageFrom() != null) {
            bundle.putInt("milleageFrom", getMilleageFrom());
        }
        if (getMilleageTo() != null) {
            bundle.putInt("milleageTo", getMilleageTo());
        }
        if (getPowerFrom() != null) {
            bundle.putInt("powerFrom", getPowerFrom());
        }
        if (getPowerTo() != null) {
            bundle.putInt("powerTo", getPowerTo());
        }
        bundle.putString("drive", getDrive());
        if (getEngineSizeFrom() != null) {
            bundle.putDouble("engineSizeFrom", getEngineSizeFrom());
        }
        if (getEngineSizeTo() != null) {
            bundle.putDouble("engineSizeTo", getEngineSizeTo());
        }
        bundle.putString("damaged", damaged);

        dest.writeBundle(bundle);
    }

    public static final Creator<FilterHelper> CREATOR = new Creator<FilterHelper>() {
        @Override
        public FilterHelper createFromParcel(Parcel in) {
            return new FilterHelper(in);
        }

        @Override
        public FilterHelper[] newArray(int size) {
            return new FilterHelper[size];
        }
    };

    @Override
    public String toString() {
        return "FilterHelper{" +
                "brand='" + brand + '\'' +
                ", bodyType='" + bodyType + '\'' +
                ", priceFrom=" + priceFrom +
                ", priceTo=" + priceTo +
                ", fuel='" + fuel + '\'' +
                ", transmission='" + transmission + '\'' +
                ", yearFrom=" + yearFrom +
                ", yearTo=" + yearTo +
                ", milleageFrom=" + milleageFrom +
                ", milleageTo=" + milleageTo +
                ", powerFrom=" + powerFrom +
                ", powerTo=" + powerTo +
                ", drive='" + drive + '\'' +
                ", engineSizeFrom='" + engineSizeFrom + '\'' +
                ", engineSizeTo='" + engineSizeTo + '\'' +
                ", damaged='" + damaged + '\'' +
                '}';
    }
}
