package pl.vlados.utils;

import android.text.Editable;
import android.widget.EditText;

import java.util.List;
import java.util.stream.Collectors;

import pl.vlados.ui.custom.CustomSpinnerMarker;
import pl.vlados.ui.custom.ICustomView;
import pl.vlados.ui.custom.SpinnerEntity;

public class CustomViewHelper {

    private static CustomViewHelper instance;

    private CustomViewHelper() {
    }

    public static CustomViewHelper getInstance() {
        if (instance == null) {
            instance = new CustomViewHelper();
        }
        return instance;
    }

    public void initCustomView(ICustomView customView) {
        customView.init();
    }

    public void initCustomView(ICustomView... customViews) {
        for (ICustomView cv : customViews) {
            initCustomView(cv);
        }
    }

    public List<SpinnerEntity> createListOfSpinnerEntity(List<? extends CustomSpinnerMarker> enums) {
        List<SpinnerEntity> result = null;
        if (enums != null && enums.size() > 0) {
            result = enums.stream().map(entity -> new SpinnerEntity(entity.getId(), entity.getTitle())).collect(Collectors.toList());

        }
        return result;
    }

    public boolean validateEditText(EditText et, String title) {
        boolean validationOk = true;
        Editable editable = et.getText();
        if (editable.toString().isEmpty()) {
            et.setError(title + " is empty");
            validationOk = false;
        }
        return validationOk;
    }

    public boolean validateEditText(EditText et, String title, Integer minValue) {
        boolean validationOk = validateEditText(et, title);
        if (validationOk) {
            Editable editable = et.getText();
            if (editable.toString().length() < minValue) {
                et.setError("Min length this field is " + minValue);
                validationOk = false;
            }
        }
        return validationOk;
}

    public boolean validateEditText(EditText et, String title, Integer minValue, Integer maxValue) {
        boolean validationOk = validateEditText(et, title, minValue);
        if (validationOk) {
            Editable editable = et.getText();
            if (editable.toString().length() > maxValue) {
                et.setError("Max length this field is " + maxValue);
                validationOk = false;
            }
        }
        return validationOk;
    }
}
