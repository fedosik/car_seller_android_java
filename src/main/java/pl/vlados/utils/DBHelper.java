package pl.vlados.utils;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import pl.vlados.entity.Brand;
import pl.vlados.entity.CarOffer;
import pl.vlados.entity.Model;

public class DBHelper extends SQLiteOpenHelper {

    private Context context;

    public DBHelper(@Nullable Context context) {
        super(context, "car_seller_db", null, 6);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.println(Log.WARN, "database","Database onCreate()");

        db.execSQL(Brand.CREATE_TABLE_BRAND);
        db.execSQL(Model.CREATE_TABLE_MODEL);
        db.execSQL(CarOffer.CREATE_TABLE_CAROFFER);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Brand.TABLE_NAME_BRAND);
        db.execSQL("DROP TABLE IF EXISTS " + Model.TABLE_NAME_MODEL);
        db.execSQL("DROP TABLE IF EXISTS " + CarOffer.TABLE_NAME_CAROFFER);

        db.execSQL(Brand.CREATE_TABLE_BRAND);
        db.execSQL(Model.CREATE_TABLE_MODEL);
        db.execSQL(CarOffer.CREATE_TABLE_CAROFFER);
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
