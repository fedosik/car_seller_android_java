package pl.vlados.enums;

public enum CarOfferStatus {

    CREATED("CA","Созданное"),
    ACTIVE("AC","Активное"),
    INACTIVE("IA","Неактивное"),
    FINISHED("FI", "Завершенное");

    String id;
    String name;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    CarOfferStatus(String id, String name) {
        this.id = id;
        this.name = name;
    }




}
