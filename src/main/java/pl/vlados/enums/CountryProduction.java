package pl.vlados.enums;

import java.util.Arrays;
import java.util.List;

import pl.vlados.ui.custom.CustomSpinnerMarker;

public enum CountryProduction implements CustomSpinnerMarker {

    JAPAN("Япония"),
    USA("США"),
    GERMANY("Германия"),
    FRANCE("Франция"),
    SOUTH_KOREA("Корея"),
    CHINA("Китай"),
    CZECH_REPUBLIC("Чехия"),
    ITALY("Италия"),
    SWEDEN("Швеция"),
    RUSSIA("Россия"),
    UKRAINE("Украина"),
    SPAIN("Испания"),
    OTHER("Другая");

    String name;

    CountryProduction(String name) {
        this.name = name;
    }

    public static CountryProduction getEnumByValue(String value) {
        CountryProduction result = null;
        for (CountryProduction country : list) {
            if (country.getTitle().equals(value)) {
                result = country;
            }
        }
        return result;
    }

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public String getTitle() {
        return name;
    }

    public static List<CountryProduction> list = Arrays.asList(CountryProduction.values());

}
