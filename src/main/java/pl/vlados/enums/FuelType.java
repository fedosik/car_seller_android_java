package pl.vlados.enums;

import java.util.Arrays;
import java.util.List;

import pl.vlados.ui.custom.CustomSpinnerMarker;

public enum FuelType implements CustomSpinnerMarker {

    DIESEL("Дизель"),
    FUEL("Бензин"),
    GAS("GAZ"),
    FUEL_GAZ("Бензин + Газ"),
    ELECTRIC("Электричество"),
    HYBRID("Гибрид");

    String name;

    public static List<FuelType> list = Arrays.asList(FuelType.values());

    FuelType(String name) {
        this.name = name;
    }

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public String getTitle() {
        return name;
    }

    public static FuelType getEnumByValue(String value) {
        FuelType result = null;
        for (FuelType fuelType : list) {
            if (fuelType.getTitle().equals(value)) {
                result = fuelType;
            }
        }
        return result;
    }
}
