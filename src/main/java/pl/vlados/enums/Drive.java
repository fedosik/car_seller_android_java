package pl.vlados.enums;

import java.util.Arrays;
import java.util.List;

import pl.vlados.ui.custom.CustomSpinnerMarker;

public enum Drive implements CustomSpinnerMarker {

    FRONT_WHEEL("Передний"),
    REAR_WHEEL("Задний"),
    FOURWD("Подключаемый полный привод"),
    AWD("Полный");

    String name;

    public static List<Drive> list = Arrays.asList(Drive.values());

    Drive(String name){
        this.name = name;
    }

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public String getTitle() {
        return name;
    }

    public static Drive getEnumByValue(String value) {
        Drive result = null;
        for (Drive drive : list) {
            if (drive.getTitle().equals(value)) {
                result = drive;
            }
        }
        return result;
    }
}
