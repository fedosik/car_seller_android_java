package pl.vlados.enums;

import java.util.Arrays;
import java.util.List;

import pl.vlados.ui.custom.CustomSpinnerMarker;

public enum Transmission implements CustomSpinnerMarker {

    AUTOMATIC("Автомат"),
    AUTOMATIC_CVT("Вариатор/CVT"),
    AUTOMATIC_DOUBLE_CLUTCH("DSG/DCT"),
    MANUAL("Ручная"),
    TIPTRONIC("Типтроник");

    private String name;

    public static List<Transmission> list = Arrays.asList(Transmission.values());

    Transmission(String name){
        this.name = name;
    }

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public String getTitle() {
        return name;
    }

    public static Transmission getEnumByValue(String value) {
        Transmission result = null;
        for (Transmission transmission : list) {
            if (transmission.getTitle().equals(value)) {
                result = transmission;
            }
        }
        return result;
    }
}
