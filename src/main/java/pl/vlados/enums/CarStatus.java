package pl.vlados.enums;

import java.util.Arrays;
import java.util.List;

import pl.vlados.ui.custom.CustomSpinnerMarker;

public enum CarStatus implements CustomSpinnerMarker {

    NON_DAMAGED("Не повреждён"),
    DAMAGED("Поврежден");

    String name;

    CarStatus(String name){
        this.name = name;
    }

    public static List<CarStatus> list = Arrays.asList(CarStatus.values());


    @Override
    public long getId() {
        return 0;
    }

    @Override
    public String getTitle() {
        return name;
    }
}
