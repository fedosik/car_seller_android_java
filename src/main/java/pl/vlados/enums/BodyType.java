package pl.vlados.enums;

import java.util.Arrays;
import java.util.List;

import pl.vlados.ui.custom.CustomSpinnerMarker;

public enum BodyType implements CustomSpinnerMarker {

    CONVERTIBLE("Кабриолет"),
    COUPE("Купэ"),
    HATCHBACK("Хэтчбек"),
    SEDAN("Седан"),
    SUV("Кроссовер/Внедорожник"),
    VAN_MINIVAN("Минивен/Микроавтобус"),
    WAGON_COMBI("Универсал/Комби");

    private String name;

    BodyType(String name){
        this.name = name;
    }

    public static List<BodyType> list = Arrays.asList(BodyType.values());

    @Override
    public long getId() {
        return 0;
    }

    @Override
    public String getTitle() {
        return name;
    }

    public static BodyType getEnumByValue(String value) {
        BodyType result = null;
        for (BodyType bodyType : list) {
            if (bodyType.getTitle().equals(value)) {
                result = bodyType;
            }
        }
        return result;
    }
}
