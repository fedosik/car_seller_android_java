package pl.vlados.services;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pl.vlados.entity.Model;
import pl.vlados.utils.DBHelper;

public class ModelService implements AbstractService<Model> {

    private DBHelper dbhelper;
    private SQLiteDatabase db;

    private ContentValues cv;

    private static ModelService instance;

    public static ModelService getInstance() {
        if (instance == null) {
            instance = new ModelService();
        }
        return instance;
    }

    private ModelService(){}

    @Override
    public long save(Model model) {

        db = dbhelper.getWritableDatabase();
        cv = new ContentValues();
        long result;

        try {
            cv.put(Model.COLUMN_TITLE, model.getTitle());
            cv.put(Model.COLUMN_BRAND_ID, model.getBrand_id());
            result = db.insert(Model.TABLE_NAME_MODEL, null, cv);

            Log.println(Log.INFO, "insertData", model.toString() + " inserted to db");

        } finally {
            db.close();
        }

        return result;
    }

    @Override
    public List<Model> loadAll() {
        db = dbhelper.getReadableDatabase();

        Model model = null;
        List<Model> result = null;

        try (Cursor cursor = db.query(Model.TABLE_NAME_MODEL, null, null, null, null, null, "title")) {
            if (cursor.moveToFirst()) {
                result = new ArrayList<>();

                int idColIndex = cursor.getColumnIndex(Model.COLUMN_ID);
                int titleColIndex = cursor.getColumnIndex(Model.COLUMN_TITLE);
                int brandidColIndex = cursor.getColumnIndex(Model.COLUMN_BRAND_ID);

                do {
                    model = new Model();

                    model.setId(cursor.getLong(idColIndex));
                    model.setTitle(cursor.getString(titleColIndex));
                    model.setBrand_id(cursor.getLong(brandidColIndex));

                    result.add(model);

                } while (cursor.moveToNext());

            }
        } catch (SQLiteException e){
            e.printStackTrace();
        }  finally {
            db.close();
        }

        return result;
    }

    @Override
    public void deleteAll() {

    }

    public void setDbhelper(DBHelper dbhelper) {
        this.dbhelper = dbhelper;
    }
}
