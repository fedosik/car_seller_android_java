package pl.vlados.services;

import android.database.Cursor;

import java.util.List;

public interface AbstractService<E> {

    long save(E e);

    List<E> loadAll();

    void deleteAll();
}
