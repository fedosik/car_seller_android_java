package pl.vlados.services;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import pl.vlados.entity.Brand;
import pl.vlados.entity.CarOffer;
import pl.vlados.entity.Model;
import pl.vlados.enums.BodyType;
import pl.vlados.enums.CarOfferStatus;
import pl.vlados.enums.CarStatus;
import pl.vlados.enums.CountryProduction;
import pl.vlados.enums.Drive;
import pl.vlados.enums.FuelType;
import pl.vlados.enums.Transmission;
import pl.vlados.ui.fragments.caroffer.CarOfferDto;
import pl.vlados.utils.DBHelper;
import pl.vlados.utils.FilterHelper;

public class CarOfferService implements AbstractService<CarOffer> {

    private static CarOfferService instance;

    private DBHelper dbhelper;
    private SQLiteDatabase db;

    private ContentValues cv;

    public static CarOfferService getInstance() {
        if (instance == null) {
            instance = new CarOfferService();
        }
        return instance;
    }

    private CarOfferService() {
    }

    @Override
    public long save(CarOffer carOffer) {

        db = dbhelper.getWritableDatabase();

        cv = new ContentValues();

        long price = carOffer.getPrice();
        long milleage = carOffer.getMilleage();
        //todo mock status
        CarOfferStatus status = CarOfferStatus.CREATED;
        boolean negociation = carOffer.isNegociation();
        int year = carOffer.getYear();
        FuelType fuelType = carOffer.getFuelType();
        BodyType bodyType = carOffer.getBodyType();
        //todo mock country
        CountryProduction country = carOffer.getCountry();
        Transmission transmission = carOffer.getTransmission();
        Double engineSize = carOffer.getEngineSize();
        short power = carOffer.getPower();
        Drive drive = carOffer.getDrive();
        byte placeAmount = carOffer.getPlaceAmount();
        boolean damaged = carOffer.isDamaged();
        long model_id = carOffer.getModel_id();
        String description = carOffer.getDescription();

        cv.put(CarOffer.COLUMN_PRICE, price);
        cv.put(CarOffer.COLUMN_MILLEAGE, milleage);
        cv.put(CarOffer.COLUMN_STATUS, status.getName());
        cv.put(CarOffer.COLUMN_NEGOCIATION, negociation);
        cv.put(CarOffer.COLUMN_YEAR, year);
        cv.put(CarOffer.COLUMN_FUEL, fuelType.getTitle());
        cv.put(CarOffer.COLUMN_BODY, bodyType.getTitle());
        cv.put(CarOffer.COLUMN_COUNTRY, CountryProduction.JAPAN.getTitle());
        cv.put(CarOffer.COLUMN_TRANSMISSION, transmission.getTitle());
        cv.put(CarOffer.COLUMN_ENGINESIZE, engineSize);
        cv.put(CarOffer.COLUMN_POWER, power);
        cv.put(CarOffer.COLUMN_DRIVE, drive.getTitle());
        cv.put(CarOffer.COLUMN_PLACEAMOUNT, placeAmount);
        cv.put(CarOffer.COLUMN_DAMAGED, damaged);
        cv.put(CarOffer.COLUMN_MODELID, model_id);
        cv.put(CarOffer.COLUMN_DESCRIPTION, description);

        long result = db.insert(CarOffer.TABLE_NAME_CAROFFER, null, cv);
        Log.println(Log.INFO, "SAVE_caroffer", carOffer.toString() + " saved to db");

        db.close();

        return result;
    }

    @Override
    public List<CarOffer> loadAll() {
        return null;
    }

    @Override
    public void deleteAll() {
        /*db = dbhelper.getWritableDatabase();

        db.delete(TABLE_CAROFFER, null, null);
        Log.d("delete_" + TABLE_CAROFFER, "All records from " + TABLE_CAROFFER + " deleted");*/
    }

    public List<CarOfferDto> getDtoList(FilterHelper filterHelper) {

        List<CarOfferDto> result = null;
        CarOfferDto offer = null;

        db = dbhelper.getReadableDatabase();

        StringBuilder sql = new StringBuilder();

        sql.append("SELECT co.id as id, b.title as title, m.title as model, co.transmission as transmission, co.year as year, co.engineSize as engineSize, co.fuel as fuel, co.price as price FROM brand as b " +
                "INNER JOIN model as m ON b.id = m.brand_id " +
                "INNER JOIN car_offer as co ON m.id = co.model_id ");

        if (filterHelper != null) {
            sql.append(whereBuilder(Optional.of(filterHelper)));
        }

        try (Cursor cursor = db.rawQuery(sql.toString(), null)) {

            if (cursor.moveToFirst()) {

                result = new ArrayList<>();

                do {
                    offer = fillOfferDtoByCursor(cursor);
                    result.add(offer);

                } while (cursor.moveToNext());
            }
        }  catch (SQLiteException e){
            e.printStackTrace();
        } finally {
            db.close();
        }
        return result;
    }

    private String whereBuilder(Optional<FilterHelper> filterHelperOptional) {

        if (filterHelperOptional.isPresent()) {

            StringBuilder where = new StringBuilder("WHERE ");

            FilterHelper filterHelper = filterHelperOptional.get();
            String and = " AND ";
            String equally = " = ";

            String brand = filterHelper.getBrand();
            String model = filterHelper.getModel();
            String bodyType = filterHelper.getBodyType();
            int priceFrom = filterHelper.getPriceFrom();
            int priceTo = filterHelper.getPriceTo();
            String fuel = filterHelper.getFuel();
            String transmission = filterHelper.getTransmission();
            int yearFrom = filterHelper.getYearFrom();
            int yearTo = filterHelper.getYearTo();
            int milleageFrom = filterHelper.getMilleageFrom();
            int milleageTo = filterHelper.getMilleageTo();
            int powerFrom = filterHelper.getPowerFrom();
            int powerTo = filterHelper.getPowerTo();
            String drive = filterHelper.getDrive();
            double engineSizeFrom = filterHelper.getEngineSizeFrom();
            double engineSizeTo = filterHelper.getEngineSizeTo();
            String damageStatus = filterHelper.getDamaged();

            if (brand != null && brand.length() > 0 && !brand.equals("ALL")) {
                where.append("b." + Brand.COLUMN_TITLE).append(equally).append("'").append(brand).append("'");
                where.append(and);
            }

            if (model != null && model.length() > 0 && !model.equals("ALL")) {
                where.append("m." + Model.COLUMN_TITLE).append(equally).append("'").append(model).append("'");
                where.append(and);
            }

            if (bodyType != null && bodyType.length() > 0 && !bodyType.equals("ALL")) {
                where.append(CarOffer.COLUMN_BODY).append(equally).append("'").append(bodyType).append("'");
                where.append(and);
            }

            if (priceFrom > 0) {
                where.append(CarOffer.COLUMN_PRICE + " >= ").append(priceFrom);
                where.append(and);
            }

            if (priceTo > 0) {
                where.append(CarOffer.COLUMN_PRICE + " <= ").append(priceTo);
                where.append(and);
            }

            if (fuel != null && fuel.length() > 0 && !fuel.equals("ALL")) {
                where.append(CarOffer.COLUMN_FUEL).append(equally).append("'").append(fuel).append("'");
                where.append(and);
            }

            if (transmission != null && transmission.length() > 0 && !transmission.equals("ALL")) {
                where.append(CarOffer.COLUMN_TRANSMISSION).append(equally).append("'").append(transmission).append("'");
                where.append(and);
            }

            if (yearFrom > 0) {
                where.append(CarOffer.COLUMN_YEAR + " >= ").append(yearFrom);
                where.append(and);
            }

            if (yearTo > 0) {
                where.append(CarOffer.COLUMN_YEAR + " <= ").append(yearTo);
                where.append(and);
            }

            if (milleageFrom > 0) {
                where.append(CarOffer.COLUMN_MILLEAGE + " >= ").append(milleageFrom);
                where.append(and);
            }

            if (milleageTo > 0) {
                where.append(CarOffer.COLUMN_MILLEAGE + " <= ").append(milleageTo);
                where.append(and);
            }

            if (powerFrom > 0) {
                where.append(CarOffer.COLUMN_POWER + " >= ").append(powerFrom);
                where.append(and);
            }

            if (powerTo > 0) {
                where.append(CarOffer.COLUMN_POWER + " <= ").append(powerTo);
                where.append(and);
            }

            if (drive != null && drive.length() > 0 && !drive.equals("ALL")) {
                where.append(CarOffer.COLUMN_DRIVE).append(equally).append("'").append(drive).append("'");
                where.append(and);
            }

            if (engineSizeFrom > 0) {
                where.append(CarOffer.COLUMN_ENGINESIZE).append(">=").append(engineSizeTo);
                where.append(and);
            }

            if (engineSizeTo > 0) {
                where.append(CarOffer.COLUMN_ENGINESIZE).append("<=").append(engineSizeTo);
                where.append(and);
            }

            if (damageStatus != null && damageStatus.length() > 0 && !damageStatus.equals("ALL")) {
                where.append(CarOffer.COLUMN_DAMAGED).append(equally).append(damageStatus.equals(CarStatus.NON_DAMAGED.getTitle()) ? 0 : 1);
                where.append(and);
            }

            where.append("1=1");
            where.append(";");

            String result = where.toString();

            Log.println(Log.DEBUG, "query", ".CarOfferService whereBuilder() " + result);

            return where.toString();
        }

        return ";";
    }

    public void setDbhelper(DBHelper dbhelper) {
        this.dbhelper = dbhelper;
    }

    public CarOffer getById(long id) {

        db = dbhelper.getReadableDatabase();

        CarOffer carOffer = null;

        try (Cursor cursor = db.query(CarOffer.TABLE_NAME_CAROFFER, null, "id = ?", new String[]{String.valueOf(id)}, null, null, null)) {
            if (cursor.moveToFirst()) {

                do {
                    carOffer = fillOfferByCursor(cursor);

                } while (cursor.moveToNext());

            }
        } finally {
            db.close();
        }
        return carOffer;
    }

    public CarOffer fillOfferByCursor(Cursor cursor) {

        CarOffer carOffer = null;

        if (cursor != null) {

            carOffer = new CarOffer();

            carOffer.setId(cursor.getLong(cursor.getColumnIndex(CarOffer.COLUMN_ID)));
            carOffer.setPrice(cursor.getLong(cursor.getColumnIndex(CarOffer.COLUMN_PRICE)));
            carOffer.setMilleage(cursor.getLong(cursor.getColumnIndex(CarOffer.COLUMN_MILLEAGE)));
            carOffer.setNegociation(Boolean.getBoolean(cursor.getString(cursor.getColumnIndex(CarOffer.COLUMN_NEGOCIATION))));
            carOffer.setYear(cursor.getInt(cursor.getColumnIndex(CarOffer.COLUMN_YEAR)));
            carOffer.setFuelType(FuelType.getEnumByValue(cursor.getString(cursor.getColumnIndex(CarOffer.COLUMN_FUEL))));
            carOffer.setBodyType(BodyType.getEnumByValue(cursor.getString(cursor.getColumnIndex(CarOffer.COLUMN_BODY))));
            carOffer.setCountry(CountryProduction.getEnumByValue(cursor.getString(cursor.getColumnIndex(CarOffer.COLUMN_COUNTRY))));
            carOffer.setTransmission(Transmission.getEnumByValue(cursor.getString(cursor.getColumnIndex(CarOffer.COLUMN_TRANSMISSION))));
            carOffer.setEngineSize(cursor.getDouble(cursor.getColumnIndex(CarOffer.COLUMN_ENGINESIZE)));
            carOffer.setPower(cursor.getShort(cursor.getColumnIndex(CarOffer.COLUMN_POWER)));
            carOffer.setDrive(Drive.getEnumByValue(cursor.getString(cursor.getColumnIndex(CarOffer.COLUMN_DRIVE))));
            carOffer.setPlaceAmount((byte) cursor.getInt(cursor.getColumnIndex(CarOffer.COLUMN_PLACEAMOUNT)));
            carOffer.setDamaged(Boolean.getBoolean(cursor.getString(cursor.getColumnIndex(CarOffer.COLUMN_DAMAGED))));
            carOffer.setModel_id(cursor.getLong(cursor.getColumnIndex(CarOffer.COLUMN_MODELID)));
            carOffer.setDescription(cursor.getString(cursor.getColumnIndex(CarOffer.COLUMN_DESCRIPTION)));
        }
        return carOffer;
    }

    public CarOfferDto fillOfferDtoByCursor(Cursor cursor) {

        CarOfferDto dto = null;

        if (cursor != null) {

            dto = CarOfferDto.builder()
                    .id(cursor.getLong(cursor.getColumnIndex("id")))
                    .title(cursor.getString(cursor.getColumnIndex("title")))
                    .model(cursor.getString(cursor.getColumnIndex("model")))
                    .transmission(cursor.getString(cursor.getColumnIndex("transmission")))
                    .year(cursor.getInt(cursor.getColumnIndex("year")))
                    .engine(cursor.getString(cursor.getColumnIndex("engineSize")))
                    .price(cursor.getLong(cursor.getColumnIndex("price")))
                    .build();
        }
        return dto;
    }
}
