package pl.vlados.services;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pl.vlados.entity.Brand;
import pl.vlados.utils.DBHelper;

public class BrandService implements AbstractService<Brand> {

    private static BrandService instance;

    private BrandService(){}

    public static BrandService getInstance() {
        if (instance == null) {
            instance = new BrandService();
        }
        return instance;
    }

    private DBHelper dbhelper;
    private SQLiteDatabase db;

    private ContentValues cv;

    @Override
    public long save(Brand brand) {

        db = dbhelper.getWritableDatabase();
        cv = new ContentValues();
        long result;

        try {
            cv.put(Brand.COLUMN_TITLE, brand.getTitle());
            result = db.insert(Brand.TABLE_NAME_BRAND, null, cv);

            Log.println(Log.INFO, "insertData", brand.toString() + " inserted to db");

        } finally {
            db.close();
        }

        return result;
    }

    @Override
    public List<Brand> loadAll() {

        db = dbhelper.getReadableDatabase();

        Brand brand = null;
        List<Brand> result = null;

        try (Cursor cursor = db.query(Brand.TABLE_NAME_BRAND, null, null, null, null, null, "title")) {
            if (cursor.moveToFirst()) {
                result = new ArrayList<>();

                int idColIndex = cursor.getColumnIndex(Brand.COLUMN_ID);
                int titleColIndex = cursor.getColumnIndex(Brand.COLUMN_TITLE);

                do {
                    brand = new Brand();

                    brand.setId(cursor.getLong(idColIndex));
                    brand.setTitle(cursor.getString(titleColIndex));

                    result.add(brand);

                } while (cursor.moveToNext());
            }
        } catch (SQLiteException e){
            e.printStackTrace();
        } finally {
            db.close();
        }

        return result;
    }

    @Override
    public void deleteAll() {

    }

    public void setDbhelper(DBHelper dbhelper) {
        this.dbhelper = dbhelper;
    }
}
