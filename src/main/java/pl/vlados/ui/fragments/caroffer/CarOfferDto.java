package pl.vlados.ui.fragments.caroffer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class CarOfferDto {

    private long id;
    private String title;
    private String model;
    private String transmission;
    private int year;
    private String engine;
    private long price;

}
