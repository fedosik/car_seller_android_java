package pl.vlados.ui.fragments.caroffer;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import pl.vlados.ui.activity.R;
import pl.vlados.utils.BaseFragment;
import pl.vlados.utils.FilterHelper;

import static pl.vlados.ui.activity.MainActivity.carOfferService;

public class CarOffersFragment extends BaseFragment {

    @BindView(R.id.car_offers_list)
    RecyclerView carOfferList;

    private List<CarOfferDto> offersList = new ArrayList<>();
    private CarOfferAdapter adapter;

    private FilterHelper filter;

    public CarOffersFragment() {
    }

    @Override
    protected int getLayoutId() {
        return R.layout.car_offers_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new CarOfferAdapter(offersList);
        carOfferList.setAdapter(adapter);
        carOfferList.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        getOffersByFilter();
    }

    private void getOffersByFilter() {

        List<CarOfferDto> offers = null;
        offers = carOfferService.getDtoList(filter);

        offersList.clear();

        if (offers != null && offers.size() > 0) {
            offersList.addAll(offers);
        }

        adapter.notifyDataSetChanged();
    }

    public void setFilter(FilterHelper filter) {
        this.filter = filter;
    }
}
