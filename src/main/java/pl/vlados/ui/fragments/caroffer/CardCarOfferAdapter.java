package pl.vlados.ui.fragments.caroffer;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.vlados.ui.activity.R;

public class CardCarOfferAdapter extends RecyclerView.Adapter<CardCarOfferAdapter.Holder> {
    private List<CarOfferDto> carOffers;

    public CardCarOfferAdapter(List<CarOfferDto> carOffers) {
        this.carOffers = carOffers;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_card, parent, false);

        return new Holder(cv);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        CarOfferDto offer = carOffers.get(position);
        holder.brand_model.setText(offer.getTitle() + " " + offer.getModel());
        holder.year.setText(String.valueOf(offer.getYear()));
        holder.price.setText(String.valueOf(offer.getPrice()));
        Log.println(Log.INFO, "holder.offer", offer.toString());
        //todo к реализации set holder.titleView.setText(offer.getTitle());

    }

    @Override
    public int getItemCount() {
        return carOffers.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        private CardView cardView;

        @BindView(R.id.item_list_card_brand_model)
        TextView brand_model;

        @BindView(R.id.item_list_card_year)
        TextView year;

        @BindView(R.id.item_list_price)
        TextView price;

        public Holder(@NonNull CardView itemView) {
            super(itemView);
            cardView = itemView;
            ButterKnife.bind(this, itemView);
        }
    }
}
