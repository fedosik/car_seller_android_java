package pl.vlados.ui.fragments.caroffer;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.vlados.ui.activity.OfferDetails;
import pl.vlados.ui.activity.R;

public class CarOfferAdapter extends RecyclerView.Adapter<CarOfferAdapter.Holder> {

    private List<CarOfferDto> carOffers;

    public CarOfferAdapter(List<CarOfferDto> carOffers) {
        this.carOffers = carOffers;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        CarOfferDto offer = carOffers.get(position);
        holder.titleView.setText(offer.getTitle() + " " + offer.getModel());
        holder.yearView.setText(String.valueOf(offer.getYear()));
        holder.priceView.setText(String.valueOf(offer.getPrice()));
        holder.transmissionView.setText(offer.getTransmission());
        holder.engineView.setText(offer.getEngine());
        Log.println(Log.INFO, "holder.offer", offer.toString());
        //todo к реализации set holder.titleView.setText(offer.getTitle());
    }

    @Override
    public int getItemCount() {
        return carOffers.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.vTitle)
        TextView titleView;

        @BindView(R.id.vYear)
        TextView yearView;

        @BindView(R.id.vPrice)
        TextView priceView;

        @BindView(R.id.vTransmission)
        TextView transmissionView;

        @BindView(R.id.vEngine)
        TextView engineView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                CarOfferDto carOffer = carOffers.get(getAdapterPosition());

                Intent intent = new Intent(v.getContext(),OfferDetails.class);
                intent.putExtra("brand", carOffer.getTitle());
                intent.putExtra("model", carOffer.getModel());
                intent.putExtra("idOffer", carOffer.getId());
                v.getContext().startActivity(intent);
            });
        }
    }
}
