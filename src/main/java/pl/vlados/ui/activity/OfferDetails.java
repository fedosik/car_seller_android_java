package pl.vlados.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.vlados.entity.CarOffer;

import static pl.vlados.ui.activity.MainActivity.carOfferService;

public class OfferDetails extends AppCompatActivity {

    String brand;
    String model;
    long offerId;

    @BindView(R.id.actitity_offer_details_brand_model)
    TextView mDetailsBrandModel;

    @BindView(R.id.actitity_offer_details_year)
    TextView mDetailsYear;

    @BindView(R.id.actitity_offer_details_milleage)
    TextView mDetailsMilleage;

    @BindView(R.id.actitity_offer_details_fuel)
    TextView mDetailsFuel;

    @BindView(R.id.actitity_offer_details_engineSize)
    TextView mDetailsEngineSize;

    @BindView(R.id.actitity_offer_details_transmission)
    TextView mDetailsTransmission;

    @BindView(R.id.actitity_offer_details_carcass)
    TextView mDetailsCarcass;

    @BindView(R.id.actitity_offer_details_power)
    TextView mDetailsPower;

    @BindView(R.id.actitity_offer_details_drive)
    TextView mDetailsDrive;

    @BindView(R.id.actitity_offer_details_negotiation)
    TextView mDetailsNegotiation;

    @BindView(R.id.actitity_offer_details_damaged)
    TextView mDetailsDamaged;

    @BindView(R.id.actitity_offer_description)
    TextView mDetailsDescription;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        brand = intent.getStringExtra("brand");
        model = intent.getStringExtra("model");
        offerId = intent.getLongExtra("idOffer", 0);

        CarOffer carOffer = carOfferService.getById(offerId);

        mDetailsBrandModel.setText(brand + " " + model);
        mDetailsYear.setText(String.valueOf(carOffer.getYear()));
        mDetailsMilleage.setText(String.valueOf(carOffer.getMilleage()));
        mDetailsFuel.setText(carOffer.getFuelType().getTitle());
        mDetailsEngineSize.setText(String.valueOf(carOffer.getEngineSize()));
        mDetailsTransmission.setText(carOffer.getTransmission().getTitle());
        mDetailsCarcass.setText(carOffer.getBodyType().getTitle());
        mDetailsPower.setText(String.valueOf(carOffer.getPower()));
        mDetailsDrive.setText(carOffer.getDrive().getTitle());
        mDetailsNegotiation.setText(carOffer.isNegociation() ? "Yes" : "No");
        mDetailsDamaged.setText(carOffer.isDamaged() ? "Yes" : "No");
        mDetailsDescription.setText(carOffer.getDescription());
    }
}
