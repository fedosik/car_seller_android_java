package pl.vlados.ui.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import pl.vlados.ui.fragments.caroffer.CarOffersFragment;
import pl.vlados.utils.FilterHelper;

public class SearchResultsActivity extends AppCompatActivity {

    private FilterHelper filterHelper;

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        Log.println(Log.INFO, "lifecycle", ".SearchResultsActivity - onCreate()");

        if (savedInstanceState != null) {
            Log.println(Log.INFO, "lifecycle", ".SearchResultsActivity - Getting filterHelper from bundle");
            // Если активность уничтожается
            filterHelper = savedInstanceState.getParcelable("filter");
        } else {
            Log.println(Log.INFO, "lifecycle", ".SearchResultsActivity - create CarOffersFragment");

            CarOffersFragment fragment = new CarOffersFragment();
            filterHelper = getIntent().getParcelableExtra("filter");

            if (filterHelper != null) {
                Log.println(Log.DEBUG, "filter_values", ".SearchResultsActivity onCreate() - " +
                        filterHelper.toString());

                fragment.setFilter(filterHelper);
            }

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState, @NonNull PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putParcelable("filter", filterHelper);
    }
}
