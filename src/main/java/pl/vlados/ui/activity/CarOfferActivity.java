package pl.vlados.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.vlados.entity.CarOffer;
import pl.vlados.entity.Model;
import pl.vlados.enums.BodyType;
import pl.vlados.enums.Drive;
import pl.vlados.enums.FuelType;
import pl.vlados.enums.Transmission;
import pl.vlados.ui.custom.CustomSpinner;
import pl.vlados.ui.custom.SpinnerEntity;

import static pl.vlados.ui.activity.MainActivity.brands;
import static pl.vlados.ui.activity.MainActivity.carOfferService;
import static pl.vlados.ui.activity.MainActivity.carcassList;
import static pl.vlados.ui.activity.MainActivity.driveList;
import static pl.vlados.ui.activity.MainActivity.fuelList;
import static pl.vlados.ui.activity.MainActivity.models;
import static pl.vlados.ui.activity.MainActivity.transmissionList;
import static pl.vlados.ui.activity.MainActivity.viewHelper;

public class CarOfferActivity extends AppCompatActivity implements View.OnClickListener {

    CustomSpinner brandSpinner;
    CustomSpinner modelSpinner;
    CustomSpinner spinnerFuel;
    CustomSpinner spinnerCarcass;
    CustomSpinner spinnerTransmission;
    CustomSpinner spinnerDrive;

    @BindView(R.id.activity_car_offer_year)
    EditText etYear;

    @BindView(R.id.activity_car_offer_Hp)
    EditText hoursePower;

    @BindView(R.id.activity_car_offer_engine_size)
    EditText engineSizeView;

    @BindView(R.id.activity_car_offer_milleage)
    EditText milleageView;

    @BindView(R.id.activity_car_offer_price)
    EditText priceView;

    @BindView(R.id.activity_car_offer_negotiation_checkbox)
    CheckBox negotiationView;

    @BindView(R.id.activity_car_offer_damaged_checkbox)
    CheckBox damagedView;

    @BindView(R.id.activity_car_offer_description)
    EditText descriptionView;

    @BindView(R.id.activity_car_offer_brand_layout)
    LinearLayout brandL;

    @BindView(R.id.activity_car_offer_model_layout)
    LinearLayout modelL;

    @BindView(R.id.activity_car_offer_fuel_layout)
    LinearLayout fuelL;

    @BindView(R.id.activity_car_offer_carcass_layout)
    LinearLayout carcassL;

    @BindView(R.id.activity_car_offer_transmission_layout)
    LinearLayout transmissionL;

    @BindView(R.id.activity_car_offer_drive_layout)
    LinearLayout driveL;

    boolean viewsAreCreated = false;

    @BindView(R.id.btnAdd)
    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_offer);
        ButterKnife.bind(this);

        initViews();

        btnAdd.setOnClickListener(this);
    }

    public boolean validate() {
        return viewHelper.validateEditText(etYear, "year", 3, 4) && viewHelper.validateEditText(hoursePower, "power", 2, 4);
    }

    @Override
    public void onClick(View v) {
        if (validate()) {
            CarOffer carOffer = getCreatedCarOffer();
            carOfferService.save(carOffer);
        }
    }

    public CarOffer getCreatedCarOffer() {

        SpinnerEntity modelSpinnerEntity = modelSpinner.getSelectedItem();
        BodyType bodyType = BodyType.getEnumByValue(spinnerCarcass.getSelectedItem().getTitie());
        FuelType fuelType = FuelType.getEnumByValue(spinnerFuel.getSelectedItem().getTitie());
        Transmission transmission = Transmission.getEnumByValue(spinnerTransmission.getSelectedItem().getTitie());
        Drive drive = Drive.getEnumByValue(spinnerDrive.getSelectedItem().getTitie());
        int year = Integer.parseInt(etYear.getText().toString());
        short hp = Short.parseShort(hoursePower.getText().toString());
        int price = Integer.parseInt(priceView.getText().toString());
        double engineSize = Double.parseDouble(engineSizeView.getText().toString());
        boolean isDamaged = damagedView.isChecked();
        boolean isNegotiation = negotiationView.isChecked();
        int milleage = Integer.parseInt(milleageView.getText().toString());
        String description = descriptionView.getText().toString();

        return CarOffer.builder()
                .model_id(modelSpinnerEntity.getId())
                .year(year)
                .power(hp)
                .price(price)
                .fuelType(fuelType)
                .bodyType(bodyType)
                .transmission(transmission)
                .drive(drive)
                .engineSize(engineSize)
                .damaged(isDamaged)
                .negociation(isNegotiation)
                .milleage(milleage)
                .description(description)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!viewsAreCreated) {
            carcassL.addView(spinnerCarcass);
            brandL.addView(brandSpinner);
            modelL.addView(modelSpinner);
            transmissionL.addView(spinnerTransmission);
            driveL.addView(spinnerDrive);
            fuelL.addView(spinnerFuel);

            viewsAreCreated = true;
        }
    }

    public void initViews() {

        ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);

        brandSpinner = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(brands), R.id.activity_car_offer_brand, false, new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerEntity selectedItem = (SpinnerEntity) parent.getItemAtPosition(position);
                Long selectedBrandId = selectedItem.getId();

                List<Model> filteredModelList = models.stream().filter(i -> i.getBrand_id() == selectedBrandId).collect(Collectors.toList());

                if (filteredModelList.size() > 0) {
                    modelSpinner.setVisibility(View.VISIBLE);
                    modelSpinner.setDataList(viewHelper.createListOfSpinnerEntity(filteredModelList));
                    modelSpinner.refreshAdapter();
                    modelSpinner.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //nothing
            }
        }, layoutParams);
        modelSpinner = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(models), R.id.activity_car_offer_model, false, layoutParams);
        spinnerFuel = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(fuelList), R.id.activity_car_offer_fuel, false, layoutParams);
        spinnerTransmission = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(transmissionList), R.id.activity_car_offer_transmission, false, layoutParams);
        spinnerDrive = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(driveList), R.id.activity_car_offer_drive, false, layoutParams);
        spinnerCarcass = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(carcassList), R.id.activity_car_offer_carcass, false, layoutParams);

        viewHelper.initCustomView(brandSpinner, modelSpinner, spinnerFuel, spinnerTransmission, spinnerDrive, spinnerCarcass);
    }
}
