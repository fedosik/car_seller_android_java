package pl.vlados.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.vlados.entity.Brand;
import pl.vlados.entity.Model;
import pl.vlados.enums.BodyType;
import pl.vlados.enums.CarStatus;
import pl.vlados.enums.Drive;
import pl.vlados.enums.FuelType;
import pl.vlados.enums.Transmission;
import pl.vlados.services.BrandService;
import pl.vlados.services.CarOfferService;
import pl.vlados.services.ModelService;
import pl.vlados.ui.custom.CustomSpinner;
import pl.vlados.ui.custom.CustomView;
import pl.vlados.ui.custom.SpinnerEntity;
import pl.vlados.ui.fragments.caroffer.CardCarOfferAdapter;
import pl.vlados.utils.CustomViewHelper;
import pl.vlados.utils.DBHelper;
import pl.vlados.utils.FilterHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static CustomViewHelper viewHelper;

    //declare services for test data
    public static BrandService brandService;
    public static ModelService modelService;
    public static CarOfferService carOfferService;

    CustomSpinner bodyTypeSpinner;
    CustomSpinner brandSpinner;
    CustomSpinner modelSpinner;
    CustomSpinner fuelTypeSpinner;
    CustomSpinner transmissionTypeSpinner;
    CustomSpinner driveTypeSpinner;
    CustomSpinner damagedSpinner;
    CustomView<Integer> priceView;
    CustomView<Integer> yearView;
    CustomView<Integer> milleageView;
    CustomView<Integer> powerView;
    CustomView<Double> engineSizeView;

    @BindView(R.id.result_btn)
    Button resultBtn;

    @BindView(R.id.main_toolbar)
    Toolbar toolbar;

    @BindView(R.id.main_list_promotion)
    RecyclerView viewPromotionCarOffers;

    //layouts
    @BindView(R.id.bodyTypeSpinner_layout)
    LinearLayout bodyTypeL;

    @BindView(R.id.brandSpinner_layout)
    LinearLayout brandSpinnerL;

    @BindView(R.id.transmission_layout)
    LinearLayout transmissionL;

    @BindView(R.id.driveSpinner_Layout)
    LinearLayout driveSpinnerL;

    @BindView(R.id.fuelTypeSpinner_Layout)
    LinearLayout fuelTypeSpinnerL;

    @BindView(R.id.milleage_Layout)
    LinearLayout milleageL;

    @BindView(R.id.modelSpinner_Layout)
    LinearLayout modelSpinnerL;

    @BindView(R.id.power_Layout)
    LinearLayout powerL;

    @BindView(R.id.price_Layout)
    LinearLayout priceL;

    @BindView(R.id.year_Layout)
    LinearLayout yearL;

    @BindView(R.id.engine_size_Layout)
    LinearLayout engineSizeL;

    @BindView(R.id.damaged_Layout)
    LinearLayout damagedL;

    boolean viewsAreCreated = false;

    FragmentManager fragmentManager = getSupportFragmentManager();
    public static DBHelper dbHelper;

    static {
        viewHelper = CustomViewHelper.getInstance();
    }

    public static List<Brand> brands = null;
    public static List<Model> models = null;
    public static List<FuelType> fuelList = null;
    public static List<BodyType> carcassList = null;
    public static List<Transmission> transmissionList = null;
    public static List<Drive> driveList = null;
    public static List<CarStatus> statusCar = null;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();

        ButterKnife.bind(this);

        initViews();

        setSupportActionBar(toolbar);
        resultBtn.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);

        menu.add(1, 1, 1, "Edit user");
        menu.add(1, 2, 1, "Add car offer");

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.create_caroffer:
                Intent intent = new Intent(getApplicationContext(), CarOfferActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public FilterHelper createSearchParams() {

        Log.println(Log.INFO, "apps_event", "createSearchParams() - Creating filter helper ");

        String bodyType = null;
        if (bodyTypeSpinner.getSelectedItem() != null) {
            bodyType = bodyTypeSpinner.getSelectedItem().toString();
        }

        String brandType = null;
        if (brandSpinner.getSelectedItem() != null) {
            brandType = brandSpinner.getSelectedItem().toString();
        }

        String model = null;
        if (modelSpinner.getSelectedItem() != null) {
            model = modelSpinner.getSelectedItem().toString();
        }

        Integer priceFrom = priceView.getFrom();
        Integer priceTo = priceView.getTo();

        Integer yearFrom = yearView.getFrom();
        Integer yearTo = yearView.getTo();

        Integer milleageFrom = milleageView.getFrom();
        Integer milleageTo = milleageView.getTo();

        Integer powerFrom = powerView.getFrom();
        Integer powerTo = powerView.getTo();

        String fuelType = null;
        if (fuelTypeSpinner.getSelectedItem() != null) {
            fuelType = fuelTypeSpinner.getSelectedItem().toString();
        }

        String transmissionType = null;
        if (transmissionTypeSpinner.getSelectedItem() != null) {
            transmissionType = transmissionTypeSpinner.getSelectedItem().toString();
        }

        String driveType = null;
        if (driveTypeSpinner.getSelectedItem() != null) {
            driveType = driveTypeSpinner.getSelectedItem().toString();
        }

        String damageStatus = null;
        if (damagedSpinner.getSelectedItem() != null) {
            damageStatus = damagedSpinner.getSelectedItem().toString();
        }

        Double engineSizeFrom = engineSizeView.getFrom();
        Double engineSizeTo = engineSizeView.getTo();

        FilterHelper filter = FilterHelper.builder()
                .bodyType(bodyType)
                .brand(brandType)
                .model(model)
                .priceFrom(priceFrom)
                .priceTo(priceTo)
                .yearFrom(yearFrom)
                .yearTo(yearTo)
                .milleageFrom(milleageFrom)
                .milleageTo(milleageTo)
                .damaged(damageStatus)
                .powerFrom(powerFrom)
                .powerTo(powerTo)
                .fuel(fuelType)
                .transmission(transmissionType)
                .drive(driveType)
                .engineSizeFrom(engineSizeFrom)
                .engineSizeTo(engineSizeTo)
                .build();

        Log.println(Log.DEBUG, "filter_values", "createSearchParams() - " +
                filter.toString());

        return filter;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.result_btn:
                FilterHelper filter = createSearchParams();
                Intent intent = new Intent(this, SearchResultsActivity.class);
                intent.putExtra("filter", filter);
                startActivity(intent);
                break;
        }
    }

    public void initData() {

        if (dbHelper == null) {
            dbHelper = new DBHelper(this);
        }
        if (brandService == null) {
            brandService = BrandService.getInstance();
            brandService.setDbhelper(dbHelper);
        }
        if (modelService == null) {
            modelService = ModelService.getInstance();
            modelService.setDbhelper(dbHelper);
        }
        if (carOfferService == null) {
            carOfferService = CarOfferService.getInstance();
            carOfferService.setDbhelper(dbHelper);
        }
        //insertTestData();
        if (brands == null) {
            brands = brandService.loadAll();
        }
        if (models == null) {
            models = modelService.loadAll();
        }
        if (fuelList == null) {
            fuelList = FuelType.list;
        }
        if (carcassList == null) {
            carcassList = BodyType.list;
        }
        if (transmissionList == null) {
            transmissionList = Transmission.list;
        }
        if (driveList == null) {
            driveList = Drive.list;
        }
        if (statusCar == null) {
            statusCar = CarStatus.list;
        }
    }

    public void initViews() {
        if (carOfferService.getDtoList(null) != null) {
            viewPromotionCarOffers.setAdapter(new CardCarOfferAdapter(carOfferService.getDtoList(null)));
            viewPromotionCarOffers.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);

        bodyTypeSpinner = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(carcassList), R.id.bodyTypeSpinner, true, layoutParams);
        brandSpinner = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(brands), R.id.brandSpinner, true, getOnItemSelectedListener(), layoutParams);
        modelSpinner = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(models), R.id.modelSpinner, true, layoutParams);
        fuelTypeSpinner = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(fuelList), R.id.fuelType, true, layoutParams);
        transmissionTypeSpinner = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(transmissionList), R.id.transmissionType, true, layoutParams);
        driveTypeSpinner = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(driveList), R.id.driveType, true, layoutParams);
        damagedSpinner = new CustomSpinner(this, viewHelper.createListOfSpinnerEntity(statusCar), R.id.damagedStatus, true, layoutParams);
        priceView = new CustomView<>(this, fragmentManager, "price", layoutParams);
        yearView = new CustomView<>(this, fragmentManager, "year", layoutParams);
        milleageView = new CustomView<>(this, fragmentManager, "milleage", layoutParams);
        powerView = new CustomView<>(this, fragmentManager, "power", layoutParams);
        engineSizeView = new CustomView<>(this, fragmentManager, "engine size", layoutParams);
        engineSizeView.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);

        viewHelper.initCustomView(bodyTypeSpinner, brandSpinner, fuelTypeSpinner, transmissionTypeSpinner, driveTypeSpinner, damagedSpinner, priceView, yearView, milleageView, powerView, engineSizeView);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!viewsAreCreated) {
            bodyTypeL.addView(bodyTypeSpinner);
            brandSpinnerL.addView(brandSpinner);
            modelSpinnerL.addView(modelSpinner);
            transmissionL.addView(transmissionTypeSpinner);
            driveSpinnerL.addView(driveTypeSpinner);
            fuelTypeSpinnerL.addView(fuelTypeSpinner);
            milleageL.addView(milleageView);
            powerL.addView(powerView);
            priceL.addView(priceView);
            yearL.addView(yearView);
            engineSizeL.addView(engineSizeView);
            damagedL.addView(damagedSpinner);

            viewsAreCreated = true;
        }
    }

    public AdapterView.OnItemSelectedListener getOnItemSelectedListener() {

        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SpinnerEntity selectedItem = (SpinnerEntity) parent.getItemAtPosition(position);

                long selectedBrandId = selectedItem.getId();

                List<Model> filteredModelList = models.stream().filter(i -> i.getBrand_id() == selectedBrandId).collect(Collectors.toList());

                if (filteredModelList.size() > 0) {
                    modelSpinnerL.setVisibility(View.VISIBLE);
                    modelSpinner.setDataList(viewHelper.createListOfSpinnerEntity(filteredModelList));
                    modelSpinner.refreshAdapter();
                    modelSpinner.setSelection(0);
                } else {
                    modelSpinner.clearAdapter();
                    modelSpinnerL.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //nothing
            }
        };
    }

    public void insertTestData() {

        HashMap<String, List<String>> brand_models = new HashMap<>();
        brand_models.put("Mazda", new ArrayList<>(Arrays.asList("3", "6", "CX-7", "CX-5", "CX-9", "CX-3")));
        brand_models.put("AUDI", new ArrayList<>(Arrays.asList("a3", "a6", "a8", "q3", "q5", "q7")));
        brand_models.put("Ford", new ArrayList<>(Arrays.asList("Fusion", "Escape", "C-Max", "S-Max", "Edge", "Mustang")));
        brand_models.put("Lexus", new ArrayList<>(Arrays.asList("IS", "NX", "RX")));
        brand_models.put("Hyundai", new ArrayList<>(Arrays.asList("Sonata", "Solaris", "Tucson", "Santa Fe", "i40", "i30")));
        brand_models.put("Nissan", new ArrayList<>(Arrays.asList("Murano", "Teana", "Maxima", "Note", "GT-R", "Almera")));

        for (Map.Entry<String, List<String>> listEntry : brand_models.entrySet()) {
            long brandId = brandService.save(new Brand(null, null, listEntry.getKey()));
            List<String> models = listEntry.getValue();
            Model m;
            for (String title : models) {
                m = new Model();
                m.setBrand_id(brandId);
                m.setTitle(title);
                //m.setDateCreated(LocalDate.now());
                //m.setDateModificated(LocalDate.now());
                long modelId = modelService.save(m);
            }
        }
    }
}
