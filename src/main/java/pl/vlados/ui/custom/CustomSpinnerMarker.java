package pl.vlados.ui.custom;

public interface CustomSpinnerMarker {

    public long getId();
    public String getTitle();
}
