package pl.vlados.ui.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentManager;

public class CustomView<T extends Number> extends AppCompatTextView implements ICustomView, CustomDialogInterface<T> {

    private String dialogTitle;
    private T from;
    private T to;

    private FragmentManager fragmentManager;

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomView(Context context,FragmentManager fragmentManager, String dialogTitle) {
        super(context);
        this.fragmentManager = fragmentManager;
        this.dialogTitle = dialogTitle;
    }

    public CustomView(Context context, FragmentManager fragmentManager, String dialogTitle, ViewGroup.LayoutParams layoutParams) {
        super(context);
        this.fragmentManager = fragmentManager;
        this.dialogTitle = dialogTitle;
        setLayoutParams(layoutParams);
    }

    public CustomView(Context context, FragmentManager fragmentManager, String dialogTitle, ViewGroup.LayoutParams layoutParams, ViewGroup viewGroup) {
        super(context);
        this.dialogTitle = dialogTitle;
        this.fragmentManager = fragmentManager;
        setLayoutParams(layoutParams);
        viewGroup.addView(this);
    }

    public T getFrom() {
        return from;
    }

    public T getTo() {
        return to;
    }

    @Override
    public void onPositiveClick(T from, T to) {

        super.setText("");

        StringBuilder builder = new StringBuilder();

        if (from != null) {
            this.from = from;
            builder.append("from ").append(from);
        } else {
            this.from = null;
        }

        if (to != null) {
            this.to = to;
            builder.append(" to ").append(to);
        } else {
            this.to = null;
        }

        super.setText(builder.toString());
    }

    public void setOnClickListener() {
        super.setOnClickListener(cl -> {
            CustomDialogFragment dialog = new CustomDialogFragment(this, dialogTitle);
            T from_int = getFrom();
            T to_int = getTo();

            String from_str = null;
            String to_str = null;

            if (from_int != null) {
                from_str = String.valueOf(from_int);
            }

            if (to_int != null) {
                to_str = String.valueOf(to_int);
            }
            dialog.setPreviousValues(from_str, to_str);
            dialog.show(fragmentManager, null);
        });
    }

    @Override
    public void init() {
        setOnClickListener();
    }


}
