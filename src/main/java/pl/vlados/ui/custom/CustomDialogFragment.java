package pl.vlados.ui.custom;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.vlados.ui.activity.R;

public class CustomDialogFragment extends DialogFragment implements View.OnClickListener {

    private CustomDialogInterface customDialogInterface;

    @BindView(R.id.titleDialog)
    TextView titleDialogView;

    @BindView(R.id.from_value)
    EditText from_value;

    @BindView(R.id.to_value)
    EditText to_value;

    @BindView(R.id.btn_positive)
    Button btn_pos;

    @BindView(R.id.btn_negative)
    Button btn_neg;

    String titleDialog;
    String from;
    String to;

    public CustomDialogFragment(CustomDialogInterface customDialogInterface, String title) {
        this.customDialogInterface = customDialogInterface;
        this.titleDialog = title;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_layout, null);
        ButterKnife.bind(this,view);

        btn_pos.setOnClickListener(this);
        btn_neg.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_positive:
                String from = from_value.getText().toString();
                String to = to_value.getText().toString();

                Integer fromInt = null;
                Integer toInt = null;

                if (from.length() > 0) {
                    fromInt = Integer.valueOf(from);
                }

                if (to.length() > 0) {
                    toInt = Integer.valueOf(to);
                }

                customDialogInterface.onPositiveClick(fromInt, toInt);
                dismiss();
                break;
            case R.id.btn_negative:
                dismiss();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if (titleDialog != null) {
            titleDialogView.setText("Please select " + titleDialog);
        }

        if (from != null && from.length() > 0) {
            from_value.setText(from);
        }
        if (to != null && to.length() > 0) {
            to_value.setText(to);
        }
    }

    public void setPreviousValues(String from, String to){
        setFrom(from);
        setTo(to);
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setTo(String to) {
        this.to = to;
    }
}