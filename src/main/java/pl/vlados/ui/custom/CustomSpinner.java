package pl.vlados.ui.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.AppCompatSpinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class CustomSpinner extends AppCompatSpinner implements ICustomView {

    private Context context;
    private List<SpinnerEntity> dataList = null;
    private ArrayAdapter<SpinnerEntity> adapter;

    private final static String ALL_VALUE = "ALL";

    private boolean withAllValue;

    public CustomSpinner(Context context, List<SpinnerEntity> dataList, int id, boolean withAllValue) {
        super(context);
        this.context = context;
        this.dataList = dataList;
        this.withAllValue = withAllValue;
    }

    public CustomSpinner(Context context, List<SpinnerEntity> dataList, int id, boolean withAllValue, LayoutParams layoutParams) {
        super(context);
        this.context = context;
        this.dataList = dataList;
        this.withAllValue = withAllValue;
        setId(id);
        setLayoutParams(layoutParams);
    }

    public CustomSpinner(Context context, List<SpinnerEntity> dataList, int id, boolean withAllValue, AdapterView.OnItemSelectedListener listener, LayoutParams layoutParams) {
        super(context);
        this.context = context;
        this.dataList = dataList;
        this.withAllValue = withAllValue;
        setId(id);
        setLayoutParams(layoutParams);
        setOnItemSelectedListener(listener);
    }

    public CustomSpinner(Context context, List<SpinnerEntity> dataList, int id, boolean withAllValue, AdapterView.OnItemSelectedListener listener, LayoutParams layoutParams, ViewGroup viewGroup) {
        super(context);
        this.context = context;
        this.dataList = dataList;
        this.withAllValue = withAllValue;
        setId(id);
        setLayoutParams(layoutParams);
        setOnItemSelectedListener(listener);
        viewGroup.addView(this);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void createAdapter(List<SpinnerEntity> dataList) {

        try {
            if (dataList == null) {
                throw new Exception("dataList is empty");
            }
            adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, dataList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            super.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshAdapter() {

        if (getDataList() != null && withAllValue) {
            setDataList(withAllValue());
        }

        if (adapter != null) {
            adapter.clear();
            adapter.addAll(getDataList());
            adapter.notifyDataSetChanged();
        } else {
            createAdapter(getDataList());
        }
    }

    public void clearAdapter() {
        if (adapter != null) {
            adapter.clear();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public SpinnerEntity getSelectedItem() {
        SpinnerEntity item = (SpinnerEntity) super.getSelectedItem();
        return item;
    }

    @Override
    public void init() {
        if (withAllValue) {
            setDataList(withAllValue());
        }
        createAdapter(getDataList());
    }

    public void setDataList(List<SpinnerEntity> dataList) {
        this.dataList = dataList;
    }

    public List<SpinnerEntity> getDataList() {
        return dataList;
    }

    public List<SpinnerEntity> withAllValue() {
        List<SpinnerEntity> resultlist = null;
        if (getDataList() != null) {
            resultlist = new ArrayList<>(Collections.singletonList(new SpinnerEntity(ALL_VALUE)));
            resultlist.addAll(getDataList());
        }
        return resultlist;
    }
}
