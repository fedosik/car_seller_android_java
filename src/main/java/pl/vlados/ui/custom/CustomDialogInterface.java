package pl.vlados.ui.custom;

public interface CustomDialogInterface<T> {

    void onPositiveClick( T from, T to);
}
