package pl.vlados.ui.custom;

import androidx.annotation.NonNull;

import lombok.Getter;

@Getter
public class SpinnerEntity {

    private long id;
    private String titie;
    private int additionalId;

    public SpinnerEntity(long id, String titie) {
        this.id = id;
        this.titie = titie;
    }

    public SpinnerEntity(String titie) {
        this.titie = titie;
    }

    @NonNull
    @Override
    public String toString() {
        return titie;
    }
}
